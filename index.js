// importing
const express = require('express');
const jsonServer = require('json-server');
const bodyParser = require("body-parser");
const { createProxyMiddleware } = require("http-proxy-middleware");
const fs = require("fs");
const path = require('path');
const { Builder, Capabilities } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");
let Queue = require('bull');

var capabilities = Capabilities.chrome();
let opts = new chrome.Options();
opts.addArguments('headless', "true");

// instnce from Express.js
const app = express();

// local variables
const port = 3000;
const API_PATH = '/api/v1/';
const API_ROOT = `http://localhost:${port}`;

// fixed settings
const middlewares = jsonServer.defaults();

const DATABASE_NAME = "database.json";
let apiEndpoints = jsonServer.router(path.join(__dirname, DATABASE_NAME));

let REDIS_URL = process.env.REDIS_URL || 'redis://129.146.236.244:6379';

// Create / Connect to a named work queue
let workQueue = new Queue('work', REDIS_URL);

// Env Variable
const ZEIT_SERVICE = false;

if (ZEIT_SERVICE) {
  // database.json will be created or overwritten by default.
  fs.copyFile(path.join(__dirname, DATABASE_NAME), path.join("/tmp/", DATABASE_NAME), (err) => {
    if (err) throw err;
    console.log("database.json was copied to tmp location");
  });
  apiEndpoints = jsonServer.router(path.join("/tmp/", DATABASE_NAME));
}

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// routing with Express.js
app.get('/', async (req, res) => {
  // let driver = await new Builder()
  // .forBrowser('chrome')
  // .setChromeOptions(opts)
  // .usingServer("http://localhost:4444")
  // .withCapabilities(capabilities)
  // .build();
  // chrome.setDefaultService(new chrome.ServiceBuilder('path/to/chromedriver').build());
  // try {
  //   await driver.get('https://selenium.dev');
  //   let element = await driver.getTitle();
  //   res.status(200).json({ element, query: req.q });
  // }
  // finally {
  //   await driver.quit();
  // }
  res.status(200).json({ query: req.q });
});

app.post('/druid', (req, res) => {
  res.status(200).jsonp(req.body);
});

app.use(API_PATH, middlewares);
app.use(API_PATH, apiEndpoints);

// app.use(
//   ["/1337x/*"],
//   createProxyMiddleware({
//     target: "https://www.1337x.to",
//     changeOrigin: true,
//     pathRewrite: {
//       "^/1337x": "/",
//     },
//   }),
// );

app.get('/job', async (req, res) => {
  let job = await workQueue.add({ video: 'http://example.com/video1.mov' });
  res.json({ id: job.id });
});

// Allows the client to query the state of a background job
app.get('/job/:id', async (req, res) => {
  let id = req.params.id;
  let job = await workQueue.getJob(id);
  if (job === null) {
    res.status(404).send("404");
  } else {
    let state = await job.getState();
    let progress = job._progress;
    let reason = job.failedReason;
    let value = job.returnvalue;
    res.json({ id, state, progress, reason, value }); //  
  }
});

// // You can listen to global events to get notified when jobs are processed
workQueue.on('completed', async (jobId, result) => {
  console.log(result);
  console.log(`Job completed with result jobId: ${jobId.id}`);
});

sleeper = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let maxJobsPerWorker = 50;
workQueue.process(maxJobsPerWorker, async (job, done) => {
  let progress = 0;
  const { video } = job.data;
  job.progress(1);
  let driver = await new Builder()
    .forBrowser('chrome')
    .setChromeOptions(opts)
    // .withCapabilities(capabilities)
    .build();
    job.progress(30);
  try {
    await driver.get(video);
    job.progress(80);
    let title = await driver.getTitle();
    job.progress(100);
    // or pass it a result
    done(null, { title });
  } catch (e) {
    done(new Error(e));
  }
  finally {
    await driver.quit();
  }
  // or give a error if error
  // done(new Error('error transcoding'));
}).catch((e) => console.log(`Something went wrong: ${e}`));

app.listen(port, error => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> 🌎  Listening on port %s. Open up %s in your browser.', port, API_ROOT);
  }
});
